Source: libtest-exit-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl,
                     libreturn-multilevel-perl
Standards-Version: 3.9.4
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtest-exit-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtest-exit-perl.git
Homepage: https://metacpan.org/release/Test-Exit
Testsuite: autopkgtest-pkg-perl

Package: libtest-exit-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         perl,
         libreturn-multilevel-perl
Description: Perl module for testing whether code exits without terminating
 Test::Exit provides some simple tools for testing that code does or does not
 call exit(), while stopping code that does exit at the point of the exit().
 Currently it does so by means of exceptions, so it will not function properly
 if the code under test calls exit() inside of an eval block or string.
 .
 The only criterion tested is that the supplied code does or does not call
 exit(). If the code throws an exception, the exception will be propagated and
 you will have to call it yourself. die()ing is not exiting for the purpose of
 these tests.
